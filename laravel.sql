-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 05, 2023 at 04:12 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_tokens`
--

CREATE TABLE `access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expired_at` timestamp NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `amazonpay_logs`
--

CREATE TABLE `amazonpay_logs` (
  `id` bigint UNSIGNED NOT NULL,
  `member_id` int NOT NULL,
  `booking_id` int NOT NULL,
  `checkout_session_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buyer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `refund_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `api_status_code` smallint NOT NULL,
  `api_json_response` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_error_reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_error_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_log` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` bigint UNSIGNED NOT NULL,
  `member_id` bigint NOT NULL,
  `b_tour_order_id` bigint NOT NULL,
  `tour_order_template_id` bigint NOT NULL,
  `admin_id` bigint NOT NULL,
  `discuss_method` int NOT NULL,
  `discuss_time` json NOT NULL,
  `hearing_first_time` date NOT NULL,
  `hearing_tour_purpose` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hearing_budget` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hearing_plan_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hearing_area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adult_count` int NOT NULL,
  `child_count` int NOT NULL,
  `hearing_room_allocation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hearing_meal_note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hearing_tour_note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hearing_other_note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduce_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `application_deadline` date NOT NULL,
  `proposal_status` int NOT NULL,
  `invoice_status` int NOT NULL,
  `payment_deadline` date NOT NULL,
  `payment_status` int NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_schedule_histories`
--

CREATE TABLE `b_schedule_histories` (
  `id` bigint UNSIGNED NOT NULL,
  `b_tour_order_id` bigint NOT NULL,
  `b_tour_order_schedule_id` bigint NOT NULL,
  `schedule_date` date NOT NULL,
  `is_draft_reserved` tinyint NOT NULL,
  `is_final_reserved` tinyint NOT NULL,
  `reserved_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_id` bigint NOT NULL,
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_tour_order_ares`
--

CREATE TABLE `b_tour_order_ares` (
  `id` bigint UNSIGNED NOT NULL,
  `prefecture_id` bigint NOT NULL,
  `city_id` bigint NOT NULL,
  `b_tour_order_id` bigint NOT NULL,
  `order_no` smallint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_tour_order_schedules`
--

CREATE TABLE `b_tour_order_schedules` (
  `id` bigint UNSIGNED NOT NULL,
  `b_tour_order_id` bigint NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `detail` json NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_tour_order_user_schedules`
--

CREATE TABLE `b_tour_order_user_schedules` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint NOT NULL,
  `tourname_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tourname_explain` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tourname_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edit_history` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `concept` json NOT NULL,
  `budget` json NOT NULL,
  `note` json NOT NULL,
  `contact_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_image` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `public_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tourname_image_preview` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tourname_image_thumbnail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_image_preview` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_image_thumbnail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint NOT NULL,
  `furusato_tour_city_id` bigint NOT NULL,
  `furusato_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `furusato_from_site` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_info` json NOT NULL,
  `concept_user` json NOT NULL,
  `budget_user` json NOT NULL,
  `note_user` json NOT NULL,
  `contact_content_user` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_image_user` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tour_name_user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tour_explain_user` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tour_image_user` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint UNSIGNED NOT NULL,
  `prefecture_id` bigint DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `prefecture_id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(48, 542, 'Sigrid Harris', 'Amir Hettinger', '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(52, 702, 'Ms. Naomie Keebler I', 'Jerrell Dietrich', '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(65, 228, 'Travon Jast', 'Natasha Koepp', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(99, 84, 'Giovanna Bartell', 'Prof. Mohammed Rippin', '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(115, 754, 'Ms. Daisha Schiller PhD', 'Ervin Watsica', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(126, 942, 'Brennan Nicolas', 'Ward Stoltenberg', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(137, 291, 'Sigurd Waelchi', 'Michaela Rau', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(153, 609, 'Chloe Lakin', 'Mr. Ladarius Buckridge DVM', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(238, 695, 'Lucy Nicolas', 'Lacy Larson Sr.', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(359, 490, 'Dr. Caterina Daniel', 'Christ Braun', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(416, 875, 'Brayan Barton PhD', 'Adolfo Weissnat', '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(439, 772, 'Prof. Wilber Langworth', 'Prof. Clint Stehr MD', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(485, 267, 'Horace Huels', 'Trinity Williamson', '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(498, 410, 'Trey Emard', 'Rafael Herzog', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(519, 565, 'Jerrold Schroeder IV', 'Xzavier D\'Amore', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(559, 977, 'Maximilian Denesik I', 'Prof. Ona Rolfson', '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(588, 438, 'Prof. Rosina Muller', 'Lindsay Price', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(590, 678, 'Prof. Thaddeus Willms', 'Buster Kihn', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(655, 658, 'Rosemarie Hoeger', 'Mrs. Ashley Heaney Jr.', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(667, 48, 'Isai Greenfelder', 'Duncan Batz', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(677, 673, 'Kira Durgan', 'Johnpaul Waters II', '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(770, 25, 'Granville Terry IV', 'Mr. Merle Hettinger', '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(792, 629, 'Bernadette Moen', 'Breanne Breitenberg', '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(844, 855, 'Luna Volkman', 'Osvaldo Schmeler', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(859, 900, 'Katelyn Monahan V', 'Addie Balistreri', '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(956, 882, 'Stanton Blick', 'Dr. Rosemary Nicolas IV', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(977, 259, 'Miracle Pouros V', 'Prof. Trey Fisher', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(987, 586, 'Kenyatta Rodriguez', 'Mrs. Anya Corkery', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(992, 260, 'Lenora Walker', 'Candelario Waters', '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(999, 307, 'Mrs. Magdalen Maggio', 'Miss Otilia Doyle PhD', '2023-03-05 08:54:13', '2023-03-05 08:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `full_cities`
--

CREATE TABLE `full_cities` (
  `id` bigint UNSIGNED NOT NULL,
  `prefecture_id` bigint NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `furusato_categories`
--

CREATE TABLE `furusato_categories` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `furusato_categories_map`
--

CREATE TABLE `furusato_categories_map` (
  `id` bigint UNSIGNED NOT NULL,
  `category_id` int NOT NULL,
  `commodity_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `furusato_commodities`
--

CREATE TABLE `furusato_commodities` (
  `id` bigint UNSIGNED NOT NULL,
  `full_city_id` bigint NOT NULL,
  `office_id` bigint NOT NULL,
  `status` tinyint NOT NULL,
  `master_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_sleep` tinyint NOT NULL,
  `type_food` tinyint NOT NULL,
  `type_experience` tinyint NOT NULL,
  `stocks` int NOT NULL,
  `limit_of_attendee_adults` int NOT NULL,
  `limit_of_attendee_children` int NOT NULL,
  `publish_at_mm` tinyint NOT NULL,
  `publish_at_dd` tinyint NOT NULL,
  `publish_at_time` time NOT NULL,
  `expire_at_mm` tinyint NOT NULL,
  `expire_at_dd` tinyint NOT NULL,
  `expire_at_time` time NOT NULL,
  `deleted_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `furusato_commodity_images_map`
--

CREATE TABLE `furusato_commodity_images_map` (
  `id` bigint UNSIGNED NOT NULL,
  `commodity_id` bigint NOT NULL,
  `image_url` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main_image` tinyint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `furusato_coupons`
--

CREATE TABLE `furusato_coupons` (
  `furusato_coupons` bigint UNSIGNED NOT NULL,
  `full_city_id` bigint NOT NULL,
  `status` tinyint NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `use_type` tinyint NOT NULL,
  `price` int NOT NULL,
  `commodity_id` bigint NOT NULL,
  `publish_num` int NOT NULL,
  `publish_at` datetime NOT NULL,
  `expire_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `furusato_coupon_owners_map`
--

CREATE TABLE `furusato_coupon_owners_map` (
  `id` bigint UNSIGNED NOT NULL,
  `coupon_id` bigint NOT NULL,
  `member_id` bigint NOT NULL,
  `booking_id` bigint NOT NULL,
  `used_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_payments`
--

CREATE TABLE `history_payments` (
  `id` bigint UNSIGNED NOT NULL,
  `member_id` bigint NOT NULL,
  `booking_id` bigint NOT NULL,
  `admin_id` bigint NOT NULL,
  `amount` double NOT NULL,
  `status` tinyint NOT NULL,
  `paypal_payment_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payer_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amazonpay_checkout_session_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amazonpay_charge_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amazonpay_charge_permission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amazonpay_buyer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` bigint UNSIGNED NOT NULL,
  `object_id` bigint NOT NULL,
  `owner_id` bigint NOT NULL,
  `category` int NOT NULL,
  `action` int NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `before` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `before_changes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `after` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `after_changes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint UNSIGNED NOT NULL,
  `sei` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mei` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sei_kana` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mei_kana` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `postal_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefecture_id` bigint DEFAULT NULL,
  `city_id` bigint DEFAULT NULL,
  `street` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `building` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint DEFAULT NULL,
  `gender` tinyint DEFAULT NULL,
  `introducer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmp_password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `login_fails` int DEFAULT NULL,
  `last_login_fail_at` datetime DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `sei`, `mei`, `sei_kana`, `mei_kana`, `phone`, `email`, `birthday`, `postal_code`, `address`, `prefecture_id`, `city_id`, `street`, `building`, `user_type`, `active`, `gender`, `introducer_name`, `email_verified_at`, `password`, `tmp_password`, `last_login`, `login_fails`, `last_login_fail_at`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'fdsfgsgá', 'fdsgsdgdsg', 'fsdfsdfds', 'dsgeqưgẻwg', '989548539', 'djh@gmail.com', '2023-03-22', 'HVD-452', 'dfdsfds', 1, 2, 'dafd', 'dfsfdsg', 'fdsfds', 1, 1, 'sdgdsg', '2023-03-05 15:28:09', 'ewrưèwe', 'dsàvdsfvds', '2023-03-05 15:26:25', 1, '2023-03-05 15:26:25', 'dfdsfgs', '2023-03-05 15:26:25', NULL, NULL),
(2, 'fdsfgsgá', 'fdsgsdgdsg', 'fsdfsdfds', 'dsgeqưgẻwg', '989548539', 'djh@gmail.com', '2023-03-22', 'HVD-452', 'dfdsfds', 1, 2, 'dafd', 'dfsfdsg', 'fdsfds', 1, 1, 'sdgdsg', '2023-03-05 15:29:48', 'ewrưèwe', 'dsàvdsfvds', '2023-03-05 15:26:25', 1, '2023-03-05 15:26:25', 'dfdsfgs', '2023-03-05 15:26:25', NULL, NULL),
(17, NULL, NULL, NULL, NULL, '1-715-241-1785', 'nora.daugherty@example.org', NULL, NULL, '15842 Wilkinson Lake Apt. 848\nEast Earlville, OR 15074', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(23, NULL, NULL, NULL, NULL, '+1-252-403-8137', 'nicolas.sophie@example.org', NULL, NULL, '662 Selmer Burgs Apt. 769\nGermaineview, TN 30084', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(78, NULL, NULL, NULL, NULL, '+1.786.853.4878', 'qdietrich@example.net', NULL, NULL, '29097 Shayna Passage\nKhalilland, TX 92709', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(134, NULL, NULL, NULL, NULL, '+1-847-752-1803', 'xlind@example.org', NULL, NULL, '947 Tremblay Streets Suite 703\nHeathcotechester, NM 94722-8895', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(138, NULL, NULL, NULL, NULL, '678.594.9925', 'vladimir75@example.net', NULL, NULL, '17593 Powlowski Walks Apt. 913\nSouth Amyberg, AR 31177-3311', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(210, NULL, NULL, NULL, NULL, '(620) 681-4038', 'goyette.deanna@example.com', NULL, NULL, '9492 Ezra Spur Apt. 605\nLake Berneiceburgh, SC 32859-5573', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(224, NULL, NULL, NULL, NULL, '(346) 995-8459', 'sim.wiza@example.net', NULL, NULL, '20386 Kemmer Alley Apt. 770\nFayfurt, MT 71494-1405', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(377, NULL, NULL, NULL, NULL, '+12203305285', 'renner.raquel@example.net', NULL, NULL, '82678 Carlie Viaduct\nWest Gillian, SD 03721', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(404, NULL, NULL, NULL, NULL, '(541) 884-5330', 'rhansen@example.com', NULL, NULL, '29729 Pouros Overpass Apt. 806\nEast Gaetano, IN 37385-4022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(462, NULL, NULL, NULL, NULL, '1-361-566-6453', 'bernard00@example.org', NULL, NULL, '812 Jaiden Pike\nKaciport, AK 87495-2198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(465, NULL, NULL, NULL, NULL, '+1 (309) 344-2314', 'eula37@example.org', NULL, NULL, '619 Aliya Station\nLednerport, OK 20798-8043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(548, NULL, NULL, NULL, NULL, '574-784-5195', 'kraig.kozey@example.net', NULL, NULL, '7726 Reid Parks\nFunkside, DE 16761-8699', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(603, NULL, NULL, NULL, NULL, '(636) 569-1932', 'clemmie25@example.com', NULL, NULL, '976 Faye Landing Suite 562\nPort Michalemouth, MI 18525-3005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(622, NULL, NULL, NULL, NULL, '+1-815-912-8570', 'elbert63@example.net', NULL, NULL, '742 Freida Village Apt. 254\nJodiestad, LA 12225', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(848, NULL, NULL, NULL, NULL, '+1 (650) 559-8623', 'hill.angie@example.com', NULL, NULL, '35531 Maymie Inlet\nLuettgenland, MA 22918-9097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(870, NULL, NULL, NULL, NULL, '(678) 650-2282', 'aylin75@example.org', NULL, NULL, '89146 Rutherford Path\nWest Eldridgeport, MD 97517-5240', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(943, NULL, NULL, NULL, NULL, '(714) 214-0537', 'kylee96@example.net', NULL, NULL, '6784 Douglas Underpass Suite 100\nEmieview, WY 53768', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(998, NULL, NULL, NULL, NULL, '872-219-7440', 'casimir.grady@example.net', NULL, NULL, '121 Casimer Ridges\nLake Lulufort, DC 83744-1563', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(32, '2019_08_19_000000_create_failed_jobs_table', 1),
(33, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(34, '2023_03_04_171844_create_table_logs_table', 1),
(35, '2023_03_04_173245_create_table_amazonpay_logs_table', 1),
(36, '2023_03_04_175010_create_table_history_payments_table', 1),
(37, '2023_03_04_175656_create_table_bookings_table', 1),
(38, '2023_03_04_180941_create_table_b_tour_order_user_schedules_table', 1),
(39, '2023_03_04_182058_create_table_schedule_templates_table', 1),
(40, '2023_03_04_182336_create_tabl_b_tour_order_ares_table', 1),
(41, '2023_03_04_182639_create_table_tour_order_templates_table', 1),
(42, '2023_03_04_183439_create_table_office_plans_table', 1),
(43, '2023_03_04_183809_create_table_offices_table', 1),
(44, '2023_03_04_184234_create_table_office_categories_table', 1),
(45, '2023_03_04_184354_create_table_furusato_commodities_table', 1),
(46, '2023_03_04_184938_create_table_furusato_categories_table', 1),
(47, '2023_03_04_185137_create_table_furusato_categories_map_table', 1),
(48, '2023_03_04_185409_create_table_furusato_commodity_images_map_table', 1),
(49, '2023_03_05_090145_create_table_paypal_logs_table', 1),
(50, '2023_03_05_090439_create_table_support_histories_table', 1),
(51, '2023_03_05_092543_create_table_tour_order_template_areas_table', 1),
(52, '2023_03_05_092806_create_table_furusato_coupons_table', 1),
(53, '2023_03_05_093326_create_table_access_tokens_table', 1),
(54, '2023_03_05_093603_create_table_sessions_table', 1),
(55, '2023_03_05_093905_create_table_notification_users_table', 1),
(56, '2023_03_05_094109_create_table_notifications_table', 1),
(57, '2023_03_05_094410_create_table_users_table', 2),
(58, '2023_03_05_095426_create_table_password_resets_table', 3),
(59, '2023_03_05_095930_create_table_members_table', 4),
(60, '2023_03_05_105811_create_table_b_tour_order_schedules_table', 5),
(61, '2023_03_05_110134_create_table_cities_table', 6),
(62, '2023_03_05_110308_create_table_prefectures_table', 7),
(63, '2023_03_05_110449_create_table_full_cities_table', 8),
(64, '2023_03_05_110753_create_table_full_post_code_table', 9),
(65, '2023_03_05_111215_create_table_furusato_coupon_owners_map_table', 10),
(66, '2023_03_05_111609_create_table_b_schedule_histories_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_users`
--

CREATE TABLE `notification_users` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NOT NULL,
  `notification_id` bigint NOT NULL,
  `booking_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` bigint UNSIGNED NOT NULL,
  `full_city_id` bigint NOT NULL,
  `category_id` bigint NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `homepage_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manager_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` mediumint NOT NULL,
  `start_work` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_work` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `office_categories`
--

CREATE TABLE `office_categories` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `office_plans`
--

CREATE TABLE `office_plans` (
  `id` bigint UNSIGNED NOT NULL,
  `office_id` bigint NOT NULL,
  `status` tinyint NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `paypal_logs`
--

CREATE TABLE `paypal_logs` (
  `id` bigint UNSIGNED NOT NULL,
  `member_id` bigint NOT NULL,
  `booking_id` bigint NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_log` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_code`
--

CREATE TABLE `post_code` (
  `id` bigint UNSIGNED NOT NULL,
  `local_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_zip_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefecture_name_kata` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_name_kata` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `town_name_kata` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefecture_name_kanji` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_name_kanji` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `town_name_kanji` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_multiple_zip_code` int NOT NULL,
  `display_small_letter` int NOT NULL,
  `display_town_area` int NOT NULL,
  `display_multiple_town_area` int NOT NULL,
  `display_update` int NOT NULL,
  `display_reason_change` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prefectures`
--

CREATE TABLE `prefectures` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prefectures`
--

INSERT INTO `prefectures` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(117, 'Erwin Upton', 'Dr. Lynn Fadel DVM', '2023-03-05 08:59:46', '2023-03-05 08:59:46'),
(230, 'Miss Sunny Wisozk', 'Bonita Gibson', '2023-03-05 08:59:46', '2023-03-05 08:59:46'),
(387, 'Jaida Lehner', 'Amy Heaney', '2023-03-05 08:59:46', '2023-03-05 08:59:46'),
(393, 'Chaya Rempel Sr.', 'Gloria Nicolas', '2023-03-05 08:59:46', '2023-03-05 08:59:46'),
(469, 'Edna Wehner I', 'Mr. Ryley Runolfsson', '2023-03-05 08:59:46', '2023-03-05 08:59:46'),
(537, 'Delmer Gaylord', 'Assunta Tremblay', '2023-03-05 08:59:46', '2023-03-05 08:59:46'),
(718, 'Prof. Ansel Cummings V', 'Karson Johnston', '2023-03-05 08:59:46', '2023-03-05 08:59:46'),
(727, 'Dr. Zola Lindgren', 'Mr. Antwan Langosh Jr.', '2023-03-05 08:59:46', '2023-03-05 08:59:46'),
(765, 'Jaleel Simonis Sr.', 'Enoch Blick', '2023-03-05 08:59:46', '2023-03-05 08:59:46'),
(928, 'Prof. Calista Wolff', 'Eleanora Runolfsdottir', '2023-03-05 08:59:46', '2023-03-05 08:59:46');

-- --------------------------------------------------------

--
-- Table structure for table `schedule_templates`
--

CREATE TABLE `schedule_templates` (
  `id` bigint UNSIGNED NOT NULL,
  `tour_order_template_id` bigint NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `detail` json NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `support_histories`
--

CREATE TABLE `support_histories` (
  `id` bigint UNSIGNED NOT NULL,
  `admin_id` bigint NOT NULL,
  `member_id` bigint NOT NULL,
  `datetime` datetime NOT NULL,
  `content` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tour_order_templates`
--

CREATE TABLE `tour_order_templates` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint NOT NULL,
  `tourname_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tourname_explain` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tourname_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edit_history` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `concept` json NOT NULL,
  `budget` json NOT NULL,
  `note` json NOT NULL,
  `contact_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_image` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `public_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tourname_image_preview` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tourname_image_thumbnail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_image_preview` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_image_thumbnail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint NOT NULL,
  `furusato_tour_city_id` bigint NOT NULL,
  `furusato_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `furusato_from_site` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_info` json NOT NULL,
  `budget_remark_status` tinyint NOT NULL,
  `budget_remark` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tour_order_template_areas`
--

CREATE TABLE `tour_order_template_areas` (
  `id` bigint UNSIGNED NOT NULL,
  `tour_order_template_id` bigint NOT NULL,
  `prefecture_id` bigint NOT NULL,
  `city_id` bigint NOT NULL,
  `order_no` smallint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int NOT NULL,
  `active` tinyint NOT NULL,
  `creator_id` bigint NOT NULL,
  `updater_id` bigint NOT NULL,
  `email_verified_at` timestamp NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmp_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_fails` int NOT NULL,
  `last_login_fail_at` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `profile_image` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `role`, `active`, `creator_id`, `updater_id`, `email_verified_at`, `password`, `tmp_password`, `login_fails`, `last_login_fail_at`, `last_login`, `profile_image`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(7, 'Miss Dena Parisian', 'jody.abshire@example.com', '+16313698563', 3, 0, 505, 569, '2023-03-05 08:42:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'N<6W9)', 800, '2020-06-27 20:41:07', '1991-08-13 00:57:53', 'C:\\Users\\DH\\AppData\\Local\\Temp\\e67837592a8db0672ebb34ed7541b61f.png', 'Qf8M9TpRrN', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(8, 'Demond Treutel MD', 'ebrekke@example.net', '+1 (308) 960-7307', 3, 1, 594, 353, '2023-03-05 08:35:00', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '<!Sj,Ex5{iv\"jON4', 552, '2019-09-10 02:16:34', '1980-06-15 16:17:15', 'C:\\Users\\DH\\AppData\\Local\\Temp\\4e7b3fb248f3dd993a7b190a04cb96e8.png', 'zcvtw0rQ5s', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(36, 'Dr. Martine Stanton', 'savanna90@example.net', '+13466337689', 1, 0, 272, 69, '2023-03-05 08:35:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'D)>.wZ@FfxN', 615, '1997-10-20 21:13:17', '2000-06-15 23:30:15', 'C:\\Users\\DH\\AppData\\Local\\Temp\\75b283dff211d8036a0f7be3d5047ad4.png', 'U3uhZgZJIV', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(48, 'Jerome Dooley', 'kulas.rupert@example.org', '1-352-886-7318', 4, 0, 618, 381, '2023-03-05 09:01:59', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'rj.cW2fC7$`wNC|L~', 352, '1971-09-30 07:20:10', '2011-08-31 15:48:59', 'C:\\Users\\DH\\AppData\\Local\\Temp\\c0dceea738dce9cb86817357259b08b6.png', 'Y4lDbYwvsB', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(55, 'Dr. Cameron Johnston', 'nhermiston@example.com', '(872) 308-3434', 5, 1, 249, 175, '2023-03-05 08:33:54', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'n19SRZ$tiKZgp', 207, '2014-11-15 02:45:24', '2003-02-15 14:30:17', 'C:\\Users\\DH\\AppData\\Local\\Temp\\7f0732d916b247e05f01b17e9886fac2.png', 'IenYYq8gHm', NULL, '2023-03-05 08:34:03', '2023-03-05 08:34:03'),
(94, 'Titus Hyatt', 'renee.swaniawski@example.com', '+1 (430) 394-5103', 4, 0, 451, 94, '2023-03-05 08:59:02', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IHm*Rk734.rqn#pc*', 358, '1977-04-30 20:24:02', '1998-07-27 01:36:44', 'C:\\Users\\DH\\AppData\\Local\\Temp\\3c720a718306a1dcfbe5332a24afef55.png', '2Yv6kMGz5x', NULL, '2023-03-05 08:59:09', '2023-03-05 08:59:09'),
(122, 'Margarete Emmerich Sr.', 'carlee.hills@example.com', '+1-312-512-7302', 3, 0, 106, 381, '2023-03-05 08:58:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '<4Y5yi8Hd', 711, '1973-08-28 02:36:27', '2014-05-12 11:45:31', 'C:\\Users\\DH\\AppData\\Local\\Temp\\6e62d421528c9da37f2104f96d68bb6f.png', 'XHGGYsliiJ', NULL, '2023-03-05 08:59:09', '2023-03-05 08:59:09'),
(138, 'Dr. Caleb Lehner', 'windler.marge@example.org', '+1-412-281-9463', 1, 1, 842, 595, '2023-03-05 08:35:01', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'b:vhd,zRzYGY', 325, '2014-08-06 17:11:52', '2006-09-03 05:31:00', 'C:\\Users\\DH\\AppData\\Local\\Temp\\24b6562a4d5e2484fc48b407a5757d40.png', 'YNl98Svvjl', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(159, 'Allen Schmitt', 'dietrich.alaina@example.com', '952.997.8390', 4, 0, 342, 490, '2023-03-05 09:01:54', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '=)e*DC95S\\!G#', 723, '2001-07-04 03:37:04', '1981-11-30 10:59:58', 'C:\\Users\\DH\\AppData\\Local\\Temp\\9c14ea62b8c8c6f02f6834df43c34914.png', '7K10Nau3zb', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(182, 'Eden Satterfield', 'dooley.vada@example.org', '+1.707.923.0960', 3, 0, 500, 97, '2023-03-05 08:42:22', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'l#Md\"PE', 443, '2019-10-07 09:31:13', '2001-10-06 16:56:32', 'C:\\Users\\DH\\AppData\\Local\\Temp\\59ba8c02ccdbb5e96c17b9810e44c312.png', 'u1oXNFWHwI', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(185, 'Aimee Bode', 'hyatt.graham@example.com', '(412) 789-7565', 3, 0, 812, 322, '2023-03-05 08:35:02', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', ']!y\"F)oWF', 132, '1981-02-03 06:21:55', '2009-09-29 04:53:22', 'C:\\Users\\DH\\AppData\\Local\\Temp\\99bd521766fe00913ebf038fc3a89c25.png', 'CPKfcsEQiY', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(189, 'Dolores Simonis', 'ybergnaum@example.net', '1-281-605-4140', 5, 0, 557, 403, '2023-03-05 08:34:55', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'G.Bl&V:0TK5m\\x', 780, '2018-01-05 07:16:59', '1996-09-02 01:27:27', 'C:\\Users\\DH\\AppData\\Local\\Temp\\890cbcc79a4ca464947e9185beb8ef3b.png', 'fGaLfQebyl', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(191, 'Yasmin Stracke', 'marjory.white@example.com', '1-661-915-1227', 3, 0, 852, 511, '2023-03-05 08:34:59', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'QVml{5\\5:4;uzj7e%{2I', 590, '2012-10-15 13:37:13', '1981-12-12 07:31:52', 'C:\\Users\\DH\\AppData\\Local\\Temp\\ca567e181e4e27c312151bd0c8087798.png', 'XxkHleAvoi', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(208, 'Francesco Jast', 'bstrosin@example.net', '+1.254.608.4096', 3, 0, 473, 69, '2023-03-05 09:01:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', ':(6y-zN', 237, '2002-10-09 13:43:52', '2012-12-29 22:56:41', 'C:\\Users\\DH\\AppData\\Local\\Temp\\ea82a7c78d821daaa454d0cbacb35b12.png', '9lv2gBuOg7', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(210, 'Dr. Anjali Hamill V', 'sedrick12@example.org', '+1-941-972-5210', 4, 0, 195, 215, '2023-03-05 08:54:08', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 't6X9CIX|d<', 757, '2006-08-23 17:12:55', '2005-02-22 13:24:01', 'C:\\Users\\DH\\AppData\\Local\\Temp\\2f017fd3215e4e2823626326a2e60d3e.png', 'HYqsONl0XE', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(236, 'Dr. Daren Hill I', 'noelia93@example.org', '(678) 317-0146', 4, 0, 80, 333, '2023-03-05 08:59:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '~j9zGR#', 617, '1987-03-29 22:58:10', '2000-03-30 01:29:19', 'C:\\Users\\DH\\AppData\\Local\\Temp\\e41547ccbe978a3e738f07663d4c5c47.png', '0HJYtRbWFM', NULL, '2023-03-05 08:59:09', '2023-03-05 08:59:09'),
(246, 'Makenzie Cruickshank', 'jaylan14@example.net', '(937) 396-7351', 4, 0, 719, 578, '2023-03-05 08:54:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'JxJ6hNH0', 364, '1992-12-08 00:53:55', '1999-12-21 06:20:41', 'C:\\Users\\DH\\AppData\\Local\\Temp\\9d47702f5ccb7a681321ed165fd2af2d.png', '4hBRgwKSMb', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(254, 'Harley Rowe', 'waino88@example.net', '(657) 685-6798', 1, 1, 97, 680, '2023-03-05 08:54:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'u<YJ[Y]>}ywTg=91]y*', 403, '1996-03-26 08:59:11', '1970-07-20 14:48:38', 'C:\\Users\\DH\\AppData\\Local\\Temp\\deaec15d5ab33041c6083adf2eba8808.png', '9bvurtFqey', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(271, 'Katelin Kuhic', 'gquitzon@example.com', '+1-573-753-9555', 2, 1, 727, 714, '2023-03-05 08:59:05', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '$TaduM65pu]l5sv', 505, '1984-07-10 03:31:13', '1995-02-12 21:14:17', 'C:\\Users\\DH\\AppData\\Local\\Temp\\37ce256290bd1dab441e44d0a6cde269.png', 'kBlnkyAdM2', NULL, '2023-03-05 08:59:09', '2023-03-05 08:59:09'),
(305, 'Madilyn Koelpin', 'feest.herbert@example.org', '+1-540-883-9740', 5, 0, 445, 485, '2023-03-05 08:42:27', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', ']f[I[hLI,/Hv7zb', 715, '1988-06-17 17:05:49', '2012-11-12 05:55:12', 'C:\\Users\\DH\\AppData\\Local\\Temp\\907d0ee645e70998943b3496dfa85437.png', 'bb5oydHeMl', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(380, 'Jerel Conroy', 'joannie.schulist@example.com', '+1-941-889-3015', 5, 0, 278, 701, '2023-03-05 09:01:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'T9vLv~[-kYm', 909, '2021-12-17 18:01:37', '1990-04-03 01:05:54', 'C:\\Users\\DH\\AppData\\Local\\Temp\\5b7d4f8e0301c8164978b09b6995ce87.png', '6bI66aAPcq', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(442, 'Dr. Okey Gutmann', 'lhamill@example.com', '1-661-640-2115', 3, 1, 416, 624, '2023-03-05 08:34:56', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '=iFU\\EpQvdI/k!d&i\\|H', 161, '2012-12-08 14:01:48', '1974-07-16 20:24:41', 'C:\\Users\\DH\\AppData\\Local\\Temp\\5968d6df4856e93d3a487459905eeea6.png', 'OiDMgIvJgg', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(451, 'Jerome Witting', 'thompson.randy@example.org', '1-770-471-8145', 3, 0, 720, 31, '2023-03-05 08:54:05', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'JJ-ZWkVA]H*/Icl', 585, '2010-05-09 03:47:48', '1998-09-29 08:04:57', 'C:\\Users\\DH\\AppData\\Local\\Temp\\e5e9d2f684814c887e376ea67e6c0b4d.png', 'FzzZ5tEPgB', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(464, 'Elfrieda Collins', 'kemmer.tiara@example.net', '360.217.5658', 3, 1, 42, 28, '2023-03-05 08:42:29', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FQQ~zm\\,ra', 171, '1989-07-11 18:22:45', '2003-11-04 03:58:00', 'C:\\Users\\DH\\AppData\\Local\\Temp\\b782071d54a0b7cf6ce8472e8b942f80.png', '3ZEA4OmztS', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(490, 'Dr. Kacie Kris', 'amitchell@example.org', '+1.928.648.1825', 3, 1, 949, 602, '2023-03-05 09:01:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 's;Bz@5U^$O*%$', 575, '1996-12-02 20:31:24', '2010-05-06 11:36:16', 'C:\\Users\\DH\\AppData\\Local\\Temp\\e9e0d4fe1017571beedc4661b9dc3645.png', 'G1pHxoqXDt', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(526, 'Everett Davis', 'emertz@example.com', '667-308-3181', 2, 1, 165, 68, '2023-03-05 09:02:01', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zrM~qTE`PE})iO\"s', 296, '1996-01-13 12:53:14', '2017-07-08 16:31:31', 'C:\\Users\\DH\\AppData\\Local\\Temp\\063df889481bcf292d867e6341735ca1.png', 'rCalsAVoDL', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(528, 'Prof. Marcel Effertz III', 'susana00@example.org', '(225) 600-7551', 2, 0, 13, 575, '2023-03-05 08:54:09', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '^|~5{bRW.b|1HAF)#z', 67, '1973-01-04 15:11:08', '2016-09-15 20:03:20', 'C:\\Users\\DH\\AppData\\Local\\Temp\\c5b8e69ea50aacb4db3e4643bbc87076.png', '3pMElBgsXa', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(543, 'Ernie Cruickshank DVM', 'jamarcus64@example.com', '+13205505462', 5, 0, 544, 404, '2023-03-05 08:42:24', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '?v{gMK', 433, '2003-05-21 23:35:04', '1979-03-29 23:18:26', 'C:\\Users\\DH\\AppData\\Local\\Temp\\447b771bc99ffbc648af57ba31248c01.png', 'qxWvbbTzzM', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(564, 'Lloyd Carter', 'aimee92@example.org', '+1 (830) 524-1879', 4, 0, 307, 821, '2023-03-05 08:59:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CsoQSP4+', 599, '1980-07-30 06:38:32', '1979-01-20 03:44:45', 'C:\\Users\\DH\\AppData\\Local\\Temp\\614311ce6f86e3bb460707a7d22bb629.png', 'ifQR77dv4W', NULL, '2023-03-05 08:59:09', '2023-03-05 08:59:09'),
(577, 'Dr. Zane Cassin', 'tbergnaum@example.net', '541-502-9049', 5, 0, 123, 713, '2023-03-05 09:01:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '!Deq,#~GDKTXr', 14, '1981-04-07 21:29:09', '1974-06-10 20:32:30', 'C:\\Users\\DH\\AppData\\Local\\Temp\\c287699bad4526061f87a5c510b4b8ec.png', 'HKdStCOMnJ', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(581, 'Dr. Samir Kris MD', 'okuneva.payton@example.org', '+1 (872) 476-3869', 3, 0, 636, 330, '2023-03-05 08:53:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'w>TLVYfo+/=', 559, '1982-07-07 23:01:12', '1998-08-22 08:10:26', 'C:\\Users\\DH\\AppData\\Local\\Temp\\15d4bf50d8fa28f840d7868baeed734b.png', 'MWZd4ocQq3', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(583, 'Mr. Donald Rempel Jr.', 'alvena77@example.net', '+1-858-281-8104', 5, 1, 898, 669, '2023-03-05 08:54:02', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Xo5F~u9', 897, '2016-06-07 19:14:40', '1983-04-02 01:42:15', 'C:\\Users\\DH\\AppData\\Local\\Temp\\27546648e7ea758563b0f8f4a078993d.png', 'c4xdHfyMMg', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(654, 'Dr. Antonietta Brekke', 'ohessel@example.com', '+1-847-493-0193', 5, 0, 225, 900, '2023-03-05 08:42:30', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '`PN{r-&1JR7qT$Y6@*.', 842, '1986-08-01 01:37:13', '2007-03-15 20:23:58', 'C:\\Users\\DH\\AppData\\Local\\Temp\\dc0eb3d5bc4cdd69ce8b8ffcf52f7083.png', 'oKqR3curOg', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(685, 'Prof. Fidel Stark', 'paxton.hoppe@example.org', '1-734-497-7806', 5, 0, 9, 556, '2023-03-05 09:01:51', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '|=qJs\"%Ta6', 374, '2011-08-04 05:46:25', '1996-10-02 10:29:35', 'C:\\Users\\DH\\AppData\\Local\\Temp\\1a2177a6b516bab6b864bd6b486a4ff6.png', 'nyYaI1khtF', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(715, 'Clovis Brekke MD', 'yost.bernhard@example.org', '+1-234-274-9292', 4, 0, 973, 238, '2023-03-05 08:42:25', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '\'ROV\\d!l]Gk4/Nt](', 271, '2004-07-20 07:40:50', '1974-06-11 11:24:20', 'C:\\Users\\DH\\AppData\\Local\\Temp\\333450f8b8d71d7168f6209d4f2f14d8.png', 'GYNEziam8F', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(727, 'Margie Grady DVM', 'lzemlak@example.net', '+1 (857) 272-2537', 1, 0, 617, 376, '2023-03-05 08:54:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '_e7JuH', 265, '2020-10-04 00:15:03', '1994-10-27 09:41:18', 'C:\\Users\\DH\\AppData\\Local\\Temp\\b76453770621041b4d58407e3f99b9c1.png', '99gtkBrXmy', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(738, 'Mrs. Alanis Larkin MD', 'paucek.cody@example.net', '+1 (225) 663-2300', 2, 1, 421, 154, '2023-03-05 08:59:00', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Yas}>=F;iDsp!|9(_', 427, '2014-10-12 01:02:18', '2017-05-30 22:57:17', 'C:\\Users\\DH\\AppData\\Local\\Temp\\16ff3945a5b5506babb7525a0700fcd4.png', 'Yu1hoth90A', NULL, '2023-03-05 08:59:09', '2023-03-05 08:59:09'),
(748, 'Prof. Nash Ledner', 'hattie35@example.net', '503-758-4182', 5, 0, 625, 663, '2023-03-05 08:33:51', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '9|ciKxwr*eUF)gWND\\>0', 486, '2020-08-21 18:05:28', '1973-09-09 18:44:54', 'C:\\Users\\DH\\AppData\\Local\\Temp\\66f7a7e1026b95203074e8994b575bde.png', 'oMZgFA209h', NULL, '2023-03-05 08:34:03', '2023-03-05 08:34:03'),
(760, 'Mr. Wyman Hauck', 'xbalistreri@example.com', '+15715223320', 4, 1, 204, 635, '2023-03-05 09:01:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '`vVq/$xAr8P', 44, '1975-01-05 20:35:57', '1987-06-09 04:10:26', 'C:\\Users\\DH\\AppData\\Local\\Temp\\5dd37f844997dd6dc8591116ad2996c7.png', 'Q7zsrvRugl', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(788, 'Julia Fritsch I', 'lamont.parker@example.net', '+16419430098', 1, 0, 457, 336, '2023-03-05 09:01:55', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', ',2^H]\"W)\\-,Z&8', 283, '1992-09-17 20:33:52', '1993-05-30 20:50:18', 'C:\\Users\\DH\\AppData\\Local\\Temp\\14021587d90f45e9206bf3c721440bed.png', 'hHHWCIItLq', NULL, '2023-03-05 09:02:02', '2023-03-05 09:02:02'),
(792, 'Dr. Layne Emmerich III', 'yasmin09@example.com', '(863) 328-2424', 5, 0, 15, 314, '2023-03-05 08:42:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'I\"T5,-j(7&[kHmtZ\'', 391, '1984-12-18 18:35:53', '2004-11-08 09:11:12', 'C:\\Users\\DH\\AppData\\Local\\Temp\\49afe40be91f1f7f3856d4de8ef6afa3.png', 'KwgA39cenq', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(848, 'Ashleigh Bins', 'lucius47@example.org', '(678) 769-3453', 1, 1, 14, 543, '2023-03-05 08:53:47', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iT8X<nfUQx(iJS', 967, '1978-09-19 20:02:05', '1980-05-21 21:41:42', 'C:\\Users\\DH\\AppData\\Local\\Temp\\f123e6847b53dc3c327df5bffe9658f2.png', 'vHgc4DaxZC', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13'),
(867, 'Hilbert Pfannerstill Jr.', 'rkemmer@example.org', '(510) 865-2669', 2, 0, 240, 666, '2023-03-05 08:42:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '<;^??%h\"x', 280, '2007-10-11 19:59:10', '2018-06-26 17:14:46', 'C:\\Users\\DH\\AppData\\Local\\Temp\\b89aeb5709083ff76c383f00d94c8f04.png', 'vgai4MA2Ee', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(878, 'Era Swaniawski', 'gwehner@example.net', '(678) 994-7382', 5, 0, 228, 564, '2023-03-05 08:58:59', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'F!:B^HO3m0E3mSNoT%|\"', 70, '1990-04-04 12:34:04', '1976-04-25 23:07:51', 'C:\\Users\\DH\\AppData\\Local\\Temp\\df7d0c57b46d38db11bd3d305fd11b1e.png', '8nvZL1CVRU', NULL, '2023-03-05 08:59:09', '2023-03-05 08:59:09'),
(899, 'Lysanne Wyman', 'ziemann.gennaro@example.org', '480.518.2564', 2, 1, 62, 496, '2023-03-05 08:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '8+JrR8.', 662, '2008-12-27 03:12:31', '2007-06-11 19:04:38', 'C:\\Users\\DH\\AppData\\Local\\Temp\\4eb6a2f231c80aa2b3eedbca63922cec.png', 'xc9rp6bCuD', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(903, 'Kaylee Goldner Sr.', 'swaters@example.net', '+1.458.603.9060', 3, 1, 645, 464, '2023-03-05 08:34:53', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Kw3`\"XqoaH}oBr1x', 686, '1997-12-02 02:28:55', '2020-12-28 18:27:43', 'C:\\Users\\DH\\AppData\\Local\\Temp\\fa3b3ff1a906722e1e3f91025b38ca71.png', 'v0X4AZL2T0', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(919, 'Sterling Huels I', 'reece83@example.net', '1-331-257-6818', 3, 0, 210, 943, '2023-03-05 08:42:28', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iQ~>0S_?P', 104, '2012-12-04 09:34:51', '1978-07-06 13:51:44', 'C:\\Users\\DH\\AppData\\Local\\Temp\\6ccc3ac73a74f2da5dc5e19a45495224.png', 'RDgRl42Rro', NULL, '2023-03-05 08:42:34', '2023-03-05 08:42:34'),
(954, 'Sammie Okuneva DVM', 'andreane96@example.com', '(478) 376-4506', 1, 1, 37, 182, '2023-03-05 08:59:01', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '[u3MG9', 59, '1975-12-26 13:50:17', '2014-04-19 16:15:45', 'C:\\Users\\DH\\AppData\\Local\\Temp\\c9918f3d98eb4aa9339ed372ed6f279b.png', '9gi7g2emT0', NULL, '2023-03-05 08:59:09', '2023-03-05 08:59:09'),
(990, 'Bailey Keeling', 'yasmin.wisozk@example.com', '(872) 645-0525', 1, 0, 923, 818, '2023-03-05 08:34:54', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4_dg%j,}SB_LE!\\@?zt', 844, '2006-11-25 14:22:03', '2014-12-30 05:55:37', 'C:\\Users\\DH\\AppData\\Local\\Temp\\f5b0e345492ce8f2ec04e82305f41a1e.png', 'qlS86tOXKv', NULL, '2023-03-05 08:35:04', '2023-03-05 08:35:04'),
(995, 'Darrick Kertzmann', 'vstamm@example.org', '279-370-1335', 3, 0, 651, 308, '2023-03-05 08:53:52', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cO{18Dm|3:gbEl.mmj3q', 805, '2020-08-24 14:16:59', '1992-07-08 18:58:39', 'C:\\Users\\DH\\AppData\\Local\\Temp\\58c561646d28c4fdb01f1b9fe0c8646c.png', 'GnubjBrJOA', NULL, '2023-03-05 08:54:13', '2023-03-05 08:54:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_tokens`
--
ALTER TABLE `access_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amazonpay_logs`
--
ALTER TABLE `amazonpay_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_schedule_histories`
--
ALTER TABLE `b_schedule_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_tour_order_ares`
--
ALTER TABLE `b_tour_order_ares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_tour_order_schedules`
--
ALTER TABLE `b_tour_order_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_tour_order_user_schedules`
--
ALTER TABLE `b_tour_order_user_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `full_cities`
--
ALTER TABLE `full_cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `furusato_categories`
--
ALTER TABLE `furusato_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `furusato_categories_map`
--
ALTER TABLE `furusato_categories_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `furusato_commodities`
--
ALTER TABLE `furusato_commodities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `furusato_commodity_images_map`
--
ALTER TABLE `furusato_commodity_images_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `furusato_coupons`
--
ALTER TABLE `furusato_coupons`
  ADD PRIMARY KEY (`furusato_coupons`);

--
-- Indexes for table `furusato_coupon_owners_map`
--
ALTER TABLE `furusato_coupon_owners_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history_payments`
--
ALTER TABLE `history_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_users`
--
ALTER TABLE `notification_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `office_categories`
--
ALTER TABLE `office_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `office_plans`
--
ALTER TABLE `office_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paypal_logs`
--
ALTER TABLE `paypal_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `post_code`
--
ALTER TABLE `post_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prefectures`
--
ALTER TABLE `prefectures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule_templates`
--
ALTER TABLE `schedule_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_histories`
--
ALTER TABLE `support_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_order_templates`
--
ALTER TABLE `tour_order_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_order_template_areas`
--
ALTER TABLE `tour_order_template_areas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_tokens`
--
ALTER TABLE `access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `amazonpay_logs`
--
ALTER TABLE `amazonpay_logs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `b_schedule_histories`
--
ALTER TABLE `b_schedule_histories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `b_tour_order_ares`
--
ALTER TABLE `b_tour_order_ares`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `b_tour_order_schedules`
--
ALTER TABLE `b_tour_order_schedules`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `b_tour_order_user_schedules`
--
ALTER TABLE `b_tour_order_user_schedules`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `full_cities`
--
ALTER TABLE `full_cities`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `furusato_categories`
--
ALTER TABLE `furusato_categories`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `furusato_categories_map`
--
ALTER TABLE `furusato_categories_map`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `furusato_commodities`
--
ALTER TABLE `furusato_commodities`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `furusato_commodity_images_map`
--
ALTER TABLE `furusato_commodity_images_map`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `furusato_coupons`
--
ALTER TABLE `furusato_coupons`
  MODIFY `furusato_coupons` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `furusato_coupon_owners_map`
--
ALTER TABLE `furusato_coupon_owners_map`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_payments`
--
ALTER TABLE `history_payments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=999;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_users`
--
ALTER TABLE `notification_users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `office_categories`
--
ALTER TABLE `office_categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `office_plans`
--
ALTER TABLE `office_plans`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paypal_logs`
--
ALTER TABLE `paypal_logs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_code`
--
ALTER TABLE `post_code`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prefectures`
--
ALTER TABLE `prefectures`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=929;

--
-- AUTO_INCREMENT for table `schedule_templates`
--
ALTER TABLE `schedule_templates`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `support_histories`
--
ALTER TABLE `support_histories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tour_order_templates`
--
ALTER TABLE `tour_order_templates`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tour_order_template_areas`
--
ALTER TABLE `tour_order_template_areas`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
