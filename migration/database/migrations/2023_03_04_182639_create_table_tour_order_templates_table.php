<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_order_templates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',191);
            $table->bigInteger('price');
            $table->string('tourname_name',191);
            $table->text('tourname_explain');
            $table->string('tourname_image',191);
            $table->text('edit_history');
            $table->text('description');
            $table->json('concept');
            $table->json('budget');
            $table->json('note');
            $table->text('contact_content');
            $table->string('contact_image',500);
            $table->string('public_url',500);
            $table->text('tourname_image_preview');
            $table->text('tourname_image_thumbnail');
            $table->text('contact_image_preview');
            $table->text('contact_image_thumbnail');
            $table->tinyInteger('type');
            $table->bigInteger('furusato_tour_city_id');
            $table->string('furusato_code',191);
            $table->text('furusato_from_site');
            $table->json('extra_info');
            $table->tinyInteger('budget_remark_status');
            $table->text('budget_remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_order_templates');
    }
};
