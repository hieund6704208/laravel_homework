<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_schedule_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('b_tour_order_id');
            $table->bigInteger('b_tour_order_schedule_id');
            $table->date('schedule_date');
            $table->tinyInteger('is_draft_reserved');
            $table->tinyInteger('is_final_reserved');
            $table->text('reserved_content');
            $table->bigInteger('admin_id');
            $table->string('admin_name',191);
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_schedule_histories');
    }
};
