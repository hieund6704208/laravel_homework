<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_tour_order_user_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',191);
            $table->bigInteger('price');
            $table->string('tourname_name',191);
            $table->text('tourname_explain');
            $table->string('tourname_image',191);
            $table->text('edit_history');
            $table->text('description');
            $table->json('concept');
            $table->json('budget');
            $table->json('note');
            $table->text('contact_content');
            $table->string('contact_image',500);
            $table->string('public_url',500);
            $table->text('tourname_image_preview');
            $table->text('tourname_image_thumbnail');
            $table->text('contact_image_preview');
            $table->text('contact_image_thumbnail');
            $table->tinyInteger('type');
            $table->bigInteger('furusato_tour_city_id');
            $table->string('furusato_code',191);
            $table->text('furusato_from_site');
            $table->json('extra_info');
            $table->json('concept_user');
            $table->json('budget_user');
            $table->json('note_user');
            $table->text('contact_content_user');
            $table->string('contact_image_user',500);
            $table->string('tour_name_user',191);
            $table->text('tour_explain_user');
            $table->text('tour_image_user');
            $table->string('extra_name',191);
            $table->text('extra_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_tour_order_user_schedules');
    }
};
