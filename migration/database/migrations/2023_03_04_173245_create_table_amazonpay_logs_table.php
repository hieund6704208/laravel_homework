<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazonpay_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('member_id');
            $table->integer('booking_id');
            $table->string('checkout_session_id');
            $table->string('buyer_id');
            $table->string('charge_id');
            $table->string('refund_id');
            $table->double('amount');
            $table->smallInteger('api_status_code');
            $table->text('api_json_response');
            $table->string('api_error_reason');
            $table->string('api_error_message');
            $table->text('transaction_log');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amazonpay_logs');
    }
};
