<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('furusato_commodities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('full_city_id');
            $table->bigInteger('office_id');
            $table->tinyInteger('status');
            $table->string('master_code');
            $table->string('name');
            $table->integer('price');
            $table->text('description');
            $table->tinyInteger('type_sleep');
            $table->tinyInteger('type_food');
            $table->tinyInteger('type_experience');
            $table->integer('stocks');
            $table->integer('limit_of_attendee_adults');
            $table->integer('limit_of_attendee_children');
            $table->tinyInteger('publish_at_mm');
            $table->tinyInteger('publish_at_dd');
            $table->time('publish_at_time');
            $table->tinyInteger('expire_at_mm');
            $table->tinyInteger('expire_at_dd');
            $table->time('expire_at_time');
            $table->dateTime('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('furusato_commodities');
    }
};
