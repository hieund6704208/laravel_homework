<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id');
            $table->string('name',500);
            $table->string('email',191);
            $table->string('phone',191);
            $table->integer('role');
            $table->tinyInteger('active',1);
            $table->bigInteger('creator_id');
            $table->bigInteger('updater_id');
            $table->timestamp('email_verified_at');
            $table->string('password',191);
            $table->string('tmp_password',191);
            $table->integer('login_fails');
            $table->dateTime('last_login_fail_at');
            $table->dateTime('last_login');
            $table->string('profile_image',500);
            $table->string('remember_token',100);
            $table->timestamp('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
