<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sei',191);
            $table->string('mei',191);
            $table->string('sei_kana',191);
            $table->string('mei_kana',191);
            $table->string('phone',191);
            $table->string('email',191);
            $table->date('birthday');
            $table->string('postal_code',191);
            $table->string('address',191);
            $table->bigInteger('prefecture_id');
            $table->bigInteger('city_id');
            $table->string('street',191);
            $table->string('building',191);
            $table->string('user_type',191);
            $table->tinyInteger('active');
            $table->tinyInteger('gender');
            $table->string('introducer_name',191);
            $table->timestamp('email_verified_at');
            $table->string('password',191);
            $table->string('tmp_password',191);
            $table->dateTime('last_login');
            $table->integer('login_fails');
            $table->dateTime('last_login_fail_at');
            $table->string('remember_token',100);
            $table->dateTime('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
};
