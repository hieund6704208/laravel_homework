<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id');
            $table->bigInteger('booking_id');
            $table->bigInteger('admin_id');
            $table->double('amount');
            $table->tinyInteger('status');
            $table->string('paypal_payment_id',191);
            $table->string('token',191);
            $table->string('payer_id',191);
            $table->string('amazonpay_checkout_session_id');
            $table->string('amazonpay_charge_id');
            $table->string('amazonpay_charge_permission_id');
            $table->string('amazonpay_buyer_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_payments');
    }
};
