<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('office_id');
            $table->tinyInteger('status');
            $table->string('name');
            $table->integer('price');
            $table->text('description');
            $table->string('image_url',512);
            $table->dateTime('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_plans');
    }
};
