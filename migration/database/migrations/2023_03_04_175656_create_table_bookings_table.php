<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id');
            $table->bigInteger('b_tour_order_id');
            $table->bigInteger('tour_order_template_id');
            $table->bigInteger('admin_id');
            $table->integer('discuss_method');
            $table->json('discuss_time');
            $table->date('hearing_first_time');
            $table->text('hearing_tour_purpose');
            $table->string('hearing_budget',191);
            $table->string('hearing_plan_time',191);
            $table->string('hearing_area',191);
            $table->integer('adult_count');
            $table->integer('child_count');
            $table->text('hearing_room_allocation');
            $table->text('hearing_meal_note');
            $table->text('hearing_tour_note');
            $table->text('hearing_other_note');
            $table->text('introduce_info');
            $table->date('start_date');
            $table->date('end_date');
            $table->date('application_deadline');
            $table->integer('proposal_status');
            $table->integer('invoice_status');
            $table->date('payment_deadline');
            $table->integer('payment_status');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};
