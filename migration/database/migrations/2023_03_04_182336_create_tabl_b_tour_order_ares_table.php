<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_tour_order_ares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prefecture_id');
            $table->bigInteger('city_id');
            $table->bigInteger('b_tour_order_id');
            $table->smallInteger('order_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_tour_order_ares');
    }
};
