<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('furusato_coupons', function (Blueprint $table) {
            $table->bigIncrements('furusato_coupons');
            $table->bigInteger('full_city_id');
            $table->tinyInteger('status');
            $table->string('code');
            $table->string('name');
            $table->tinyInteger('use_type');
            $table->integer('price');
            $table->bigInteger('commodity_id');
            $table->integer('publish_num');
            $table->dateTime('publish_at');
            $table->dateTime('expire_at');
            $table->dateTime('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('furusato_coupons');
    }
};
