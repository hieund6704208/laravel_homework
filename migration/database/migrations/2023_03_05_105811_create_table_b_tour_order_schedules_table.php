<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_tour_order_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('b_tour_order_id');
            $table->string('title',191);
            $table->string('description',191);
            $table->date('date');
            $table->json('detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_tour_order_schedules');
    }
};
