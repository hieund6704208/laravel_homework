<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id'=>$this->faker->numberBetween(1, 1000) ,
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->phoneNumber(),
            'role' => $this->faker->numberBetween(1, 5),
            'active' => $this->faker->boolean(),
            'creator_id' => $this->faker->numberBetween(1, 1000),
            'updater_id' => $this->faker->numberBetween(1, 1000),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'tmp_password' => $this->faker->password(),
            'login_fails' => $this->faker->numberBetween(1, 1000),
            'last_login_fail_at' => $this->faker->dateTime(),
            'last_login' => $this->faker->dateTime(),
            'profile_image' => $this->faker->image(),
            'remember_token' => Str::random(10),
        ];
    }
}
